var notes = {};

exports.list = function(req, res) {
  res.json(Object.keys(notes));
}
exports.get = function(note_name, req, res) {
  if (!notes.hasOwnProperty(note_name)) {
    res.status(404);
    res.end();
    console.error(note_name + " NOT FOUND!");
  } else {
    res.json(notes[note_name]);
  }
};

exports.getByContent = function(content, req, res){
  if (content != undefined){
    for (var element in notes){
      if (notes[element]["content"] == content){
          return res.json(notes[element]);
      }
    }
    res.status(404);
    res.end();
    console.error("COULD NOT FIND THE SPECIFIED CONTENT!");
  }
  else{
    res.status(400);
    res.end();
    console.error("CONTENT VARIABLE DOES NOT EXIST!");
  }
};

exports.insert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    res.status(409);
    return console.error(note_name + " ALREADY THERE!");
  } else {
    if (!req.body.content) {
      console.log(req.body);
      res.status(422);
      res.end();
      return console.error(note_name + " INCORRECT BODY JSON!");
    }
    notes[note_name] = { content: req.body.content, inserted: new Date() };
    res.end();
  }
};

exports.upsert = function(note_name, req, res) {
  if (notes.hasOwnProperty(note_name)) {
    notes[note_name] = { content: req.body.content, modified: new Date() };
    res.end();
  } else {
    this.insert(note_name, req, res);
  }
};

exports.delete = function(note_name, req, res){
  if (notes.hasOwnProperty(note_name)){
    delete notes[note_name];
    res.status(200);
  } else {
    res.status(404);
    console.error(note_name + " DOES NOT EXIST!");
  }
  res.end();
};
